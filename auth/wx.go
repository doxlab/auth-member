package auth

import (
	"context"
	"encoding/json"
	"errors"
	"fmt"
	"log"
	"net/http"
	"strings"

	pb "gitee.com/doxlab/auth-member/proto"
	"github.com/shimingyah/pool"
	"google.golang.org/grpc/metadata"
)

/**
 * 提供给其他微服务用于微信登录的库
 *
 */

type WxAuth struct {
	IgnorePath       map[string]bool
	IgnorePathPrefix []string
	UserCtxKey       string
	Host             string
	Port             string
	pool             pool.Pool
	LoginUrl         string

}

var wxAuth *WxAuth

func NewWxAuth(host, port string) *WxAuth {
	if wxAuth == nil {
		wxAuth = &WxAuth{
			Host:       host,
			Port:       port,
			UserCtxKey: "wx_user_info",
			IgnorePath: map[string]bool{
				"/login": true,
			},
			IgnorePathPrefix: []string{
				"/static",
			},
			LoginUrl: "/login",
		}

		pool, err := pool.New(host+":"+port, pool.DefaultOptions)
		if err != nil {
			log.Fatalf("failed to new wechat auth connection pool: %v", err)
		}
		wxAuth.pool = pool
	}

	return wxAuth
}

func (o *WxAuth)HelloWorld(){
	println("hello world")
}

/**
 * 微信登录handler
 */
func (o *WxAuth) Login(w http.ResponseWriter, r *http.Request) {

	rs := &Result{}
	defer HandleResult(rs, w)
	// 获取code参数
	r.ParseForm()
	code := struct {
		Code string `json:"code"`
	}{}
	// json反序列化
	err := json.NewDecoder(r.Body).Decode(&code)
	if err != nil {
		println(err.Error())
		rs.Status = http.StatusBadRequest
		rs.Err = err
		return
	}

	// 连接GRPC
	conn, err := o.Conn()
	defer conn.Close()
	if err != nil {
		fmt.Println(err)
		w.WriteHeader(http.StatusUnauthorized)
		return
	}
	client := o.AuthClient(conn)

	ctx := metadata.NewOutgoingContext(context.Background(), map[string][]string{
		"code": {code.Code},
	})
	v, err := client.Login(ctx, &pb.WxLogin{
		Code: code.Code,
	})

	if err != nil {
		rs.Status = http.StatusBadRequest
		rs.Err = err
		return
	}

	// 登录成功后，返回V
	rs.Data = map[string]interface{}{
		"token":    v.AccessState.Token,
		"accessID": v.AccessState.AccessId,
	}

}

func (o *WxAuth) Close() error {
	return o.pool.Close()
}

func (o *WxAuth) Conn() (pool.Conn, error) {
	return o.pool.Get()
}

func (o *WxAuth) AuthClient(pc pool.Conn) pb.WxAuthServiceClient {
	return pb.NewWxAuthServiceClient(pc.Value())
}

/**
 * 从http请求头部获取token和openID
 */
func (o *WxAuth) GetTokenAndAccessID(r *http.Request) (string, string, error) {
	token := r.Header.Get("X-Token")
	accessID := r.Header.Get("X-AccessID")
	if token == "" || accessID == "" {
		log.Println("token or accessID is empty")
		return "", "", errors.New("token or accessID is empty")
	}
	return strings.TrimSpace(token), strings.TrimSpace(accessID), nil
}

/**
 * 微信身份验证handler
 */
func (o *WxAuth) ServeHTTP(rw http.ResponseWriter, r *http.Request, next http.HandlerFunc) {

	if r.URL.Path == o.LoginUrl {
		next.ServeHTTP(rw, r)
		return
	}
	token, accessID, err := o.GetTokenAndAccessID(r)
	if err != nil {
		rw.WriteHeader(http.StatusUnauthorized)
		return
	}
	// 连接GRPC
	conn, err := o.Conn()
	defer conn.Close()
	if err != nil {
		fmt.Println(err)
		rw.WriteHeader(http.StatusUnauthorized)
		return
	}
	client := o.AuthClient(conn)

	ctx := metadata.NewOutgoingContext(context.Background(), map[string][]string{
		"client":{"miniapp"},
	})
	v, err := client.CheckWxToken(ctx, &pb.WxToken{
		Token:    token,
		AccessId: accessID,
	})

	// 验证token
	if err != nil {
		log.Println("token error:",err)
		rw.WriteHeader(http.StatusUnauthorized)
		return
	}
	// TODO: 这里写错了，应该是写入response 
	// r.Header.Set("X-Token", v.AccessState.Token)
	// r.Header.Set("X-AccessID", v.AccessState.AccessId)
	info := map[string]interface{}{

		"ID":     v.MemberAccount.Id,
		"Name":   v.MemberAccount.Name,
		"Mobile": v.MemberAccount.Mobile,
		"Avatar": v.MemberAccount.Avatar,
		"OpenID": v.OpenId,
	}
	// 把info 放入context
	contx := r.Context()
	contx = context.WithValue(contx, MemberContextKey, info)
	r = r.WithContext(contx)

	if next != nil {
		next.ServeHTTP(rw, r)
	}

}

func (o *WxAuth) RegisterIgnoreURL(url string) {}
