package auth

import (
	"net/http"

	"github.com/unrolled/render"
)

type CtxKey string
const (
	MemberContextKey CtxKey = "TgMember"
)
// IKnownError 已知的预定义的错误
type IKnownError interface {
	Error() string
	ErrorCode() string
}

type WxMember struct {
	ID        int64  `json:"id"`
	Name      string `json:"name"`
	Avatar    string `json:"avatar"`
	OpenID   string `json:"openid"`
}

var jRender = render.New(render.Options{
	Charset:       "UTF-8",                                          // Sets encoding for json and html content-types. Default is "UTF-8".
	IndentJSON:    true,                                             // Output human readable JSON.
	IndentXML:     true,                                             // Output human readable XML.
	PrefixJSON:    []byte(""),                                       // Prefixes JSON responses with the given bytes.
	PrefixXML:     []byte("<?xml version='1.0' encoding='UTF-8'?>"), // Prefixes XML responses with the given bytes.
	StreamingJSON: true,
})

type QueryResult struct {
	Total int64       `json:"total"`
	Data  interface{} `json:"data"`
}

func Render() *render.Render {
	return jRender
}

type Result struct {
	Status int         `bson:"-" json:"-"`
	Err    error       `bson:"-" json:"-"`
	Msg    string      `bson:"msg" json:"msg"`
	Code   string      `bson:"code" json:"code"`
	Data   interface{} `bson:"data" json:"data"`
}

func HandleResult(rs *Result, w http.ResponseWriter) {
	if rs.Err != nil {
		if er, ok := rs.Err.(IKnownError); ok {
			rs.Status = http.StatusBadRequest
			rs.Code = er.ErrorCode()
		}
		if rs.Status == 0 {
			rs.Status = http.StatusInternalServerError
		}
		if rs.Msg == "" {
			rs.Msg = rs.Err.Error()
		}
		Render().JSON(w, rs.Status, rs)
	} else {
		if rs.Status == 0 {
			rs.Status = http.StatusOK
		}
		Render().JSON(w, rs.Status, rs.Data)
	}
}

func HandleFileResult(rs *Result, w http.ResponseWriter) {
	if rs.Err != nil {
		if er, ok := rs.Err.(IKnownError); ok {
			rs.Status = http.StatusBadRequest
			rs.Code = er.ErrorCode()
		}
		if rs.Status == 0 {
			rs.Status = http.StatusInternalServerError
		}
		if rs.Msg == "" {
			rs.Msg = rs.Err.Error()
		}
		Render().JSON(w, rs.Status, rs)
	} else {
	}
}

func GetMember(r *http.Request) map[string]interface{} {
	return r.Context().Value(MemberContextKey).(map[string]interface{})
}

// func GetUserInfo(r *http.Request) model.UserInfo {
// 	u := r.Context().Value(UserCtxKey).(*Wxmem)
// 	ui := model.UserInfo{}
// 	ui.ID = u.ID
// 	ui.Name = u.Name
// 	return ui
// }