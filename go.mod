module gitee.com/doxlab/auth-member

go 1.14

require (
	github.com/golang/protobuf v1.5.2
	github.com/shimingyah/pool v0.0.0-20190724082523-04bd98b0fbfe
	github.com/unrolled/render v1.4.1
	google.golang.org/grpc v1.46.2
)
