# tg-member配套的grpc服务访问库

1. 实现微信身份校验的中间件
2. 包含登录的逻辑
3. 包含http中间件
4. 通过grpc访问tg-member的身份校验服务
5. 提供web方式的账号密码登录
6. 支持通过grpc服务实现web方式的账号密码登录和身份验证
   
## 使用方法

微信验证:

```golang

    // 微信的身份验证方式
    import "gitee.com/doxlab/auth-member/auth"

	auth:=auth.NewWxAuth(authHost,authPort)
    // auth就可以作为中间件引入到web server中

```

